package com.gameley.entity;


import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.gameley.entity.AgDoc;
//import lombok.Data;



/**
 * 
 *
 * @author WWMXD
 * @email 309980030@qq.com
 * @date 2018-01-30 14:46:58
 */
//@Data
@TableName("ag_doc")
public class AgDoc extends Model<AgDoc> {
	private static final long serialVersionUID=1L;
		    //
    @TableId(value="id", type= IdType.AUTO)
    private Integer id;
	
		    //标题
    @TableField("title")
    private String title;
	
		    //正文
    @TableField("content")
    private String content;
	
		    //作者
    @TableField("article")
    private String article;
	
		    //审核状态
    @TableField("state")
    private Integer state;
	
		    //
    @TableField("dt")
    private Date dt;
	
		    //最后修改人
    @TableField("modifyid")
    private Integer modifyid;
	
	
		/**
	 * 设置：
	 */
	public void setId(Integer id) {
			this.id = id;
			}
	/**
	 * 获取：
	 */
	public Integer getId() {
			return id;
			}
		/**
	 * 设置：标题
	 */
	public void setTitle(String title) {
			this.title = title;
			}
	/**
	 * 获取：标题
	 */
	public String getTitle() {
			return title;
			}
		/**
	 * 设置：正文
	 */
	public void setContent(String content) {
			this.content = content;
			}
	/**
	 * 获取：正文
	 */
	public String getContent() {
			return content;
			}
		/**
	 * 设置：作者
	 */
	public void setArticle(String article) {
			this.article = article;
			}
	/**
	 * 获取：作者
	 */
	public String getArticle() {
			return article;
			}
		/**
	 * 设置：审核状态
	 */
	public void setState(Integer state) {
			this.state = state;
			}
	/**
	 * 获取：审核状态
	 */
	public Integer getState() {
			return state;
			}
		/**
	 * 设置：
	 */
	public void setDt(Date dt) {
			this.dt = dt;
			}
	/**
	 * 获取：
	 */
	public Date getDt() {
			return dt;
			}
		/**
	 * 设置：最后修改人
	 */
	public void setModifyid(Integer modifyid) {
			this.modifyid = modifyid;
			}
	/**
	 * 获取：最后修改人
	 */
	public Integer getModifyid() {
			return modifyid;
			}
		@Override
	protected Serializable pkVal() {
        return this.id;
	}
	@Override
	public String toString() {
	    return "AgDoc{"+
	"Id="+id+
	"Title="+title+
	"Content="+content+
	"Article="+article+
	"State="+state+
	"Dt="+dt+
	"Modifyid="+modifyid+
		"}";}
}